// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:khademni/ghaith/Job_offer.dart';

class JobCard extends StatelessWidget {
  final JobOffer jobOffer;

  const JobCard({Key? key, required this.jobOffer}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      color:Colors.white,
      elevation: 50.0,

      shadowColor: Colors.grey.withOpacity(0.5),
      shape: RoundedRectangleBorder(
        borderRadius:BorderRadius.circular(15.0),
         ),
         child: Padding(
           padding: const EdgeInsets.all(8.0),
           child: Row(
             children: [
               Container(
                 width: 80,
                 height: 80,
                 padding: const EdgeInsets.only(right:20),
                 decoration: BoxDecoration(
                   shape:BoxShape.circle,
                   border:Border.all(width:5,color: Color(0xFF7779F1)),
                   image:DecorationImage(
                     image: AssetImage(jobOffer.img),
                   ),
                   boxShadow: [
                     BoxShadow(
                       blurRadius: 8,
                       color: Colors.white,
                       offset: Offset(0,3),
                     ),
                   ]
                 ),

               ),
               SizedBox(
                 width: 20,
               ),
               Column(
                 children: [
                   Padding(padding: const EdgeInsets.only(top:8,bottom:8),
                   child: Text(
                     jobOffer.Company,style:TextStyle(fontSize:24,),
                   ),),
                   Padding(padding: const EdgeInsets.only(top:0,bottom:0, right: 0,),
                   child: Text(
                     jobOffer.job,style:TextStyle(fontSize:20,color: Colors.grey),
                   ),),
                   Padding(padding: const EdgeInsets.only(top:8,bottom:8),
                   child: Row(children: [
                    Icon(Icons.timer,size: 20,),
                    Text(jobOffer.clock,style:TextStyle(fontSize:20,color: Colors.grey),),
                   ],),),
                 ],
               )
             ],
           ),
         ),
      
    );
  }
}