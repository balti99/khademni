import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:khademni/ghaith/MyHomePage.dart';
import 'package:khademni/log_in.dart';
import 'package:khademni/user_model.dart';


class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);


  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();
  @override
  void initState() {
    super.initState();
    FirebaseFirestore.instance
        .collection("users")
        .doc(user!.uid)
        .get()
        .then((value) {
      this.loggedInUser = UserModel.fromMap(value.data());
      setState(() {});
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: const Text('Profile',
        style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.normal,
          ),
        ),
        centerTitle: true,
        leading: BackButton(
          onPressed: (){
            Navigator.push(context,
                MaterialPageRoute(
                    builder: (context) =>  const MyHomePage()));
          },
        ),
        actions: [
          TextButton(
            style: TextButton.styleFrom(
              primary: Colors.white,
            ) ,
            child: Text('Log out'),
            onPressed: (){
              logout(context);
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) =>  const LogInPageFinal()));
            },
          ),
        ],

      ),
        body: SingleChildScrollView(

         child: Column(
           children: <Widget>[
             Stack (

                 clipBehavior: Clip.none, alignment: Alignment.center,
                 children: <Widget> [
                   Image(
                     width : MediaQuery.of(context).size.width ,
                     height : MediaQuery.of(context).size.height / 3 ,
                     fit : BoxFit.cover,


                     image: const AssetImage(
                         "img/profile.png"
                     ),
                     ),
                   const Positioned(
                       bottom : -50.0 ,
                       child: CircleAvatar(radius: 80 ,
                           backgroundColor:Colors.white ,
                           backgroundImage : NetworkImage('https://t3.ftcdn.net/jpg/03/46/83/96/360_F_346839683_6nAPzbhpSkIpb8pmAwufkC7c5eD7wYws.webp') ))
                 ]
             ),
             SizedBox(height:50,),
             ListTile(
               title : Center(child: Text("${loggedInUser.name} ${loggedInUser.lastname}" ,  style: TextStyle(fontWeight: FontWeight.bold) ),),
               subtitle : Center(child:Text('Full Stack developer')),
             ),
             SizedBox(height: 0.0),
             SizedBox(height: 10.0,),
             ElevatedButton(onPressed: () => showModalBottomSheet(context: context,
               builder: (context) => contact(),
               shape: const RoundedRectangleBorder(
                   borderRadius: BorderRadius.vertical(
                     top: Radius.circular(20),
                   )
               ),
             ),
               style: ElevatedButton.styleFrom(
                   minimumSize: Size(300, 50),
                   primary: Colors.deepPurpleAccent,
                   onPrimary: Colors.black,
                   shape:  RoundedRectangleBorder(
                     borderRadius:  BorderRadius.circular(30.0),

                   )

               ),
               child: const Text(
                   "View Contact",
                   style: TextStyle(
                     fontSize: 26,
                     fontWeight: FontWeight.normal,
                     color: Colors.white,
                   )
               ),

             ),
             SizedBox(height: 10.0,),
             ElevatedButton(onPressed: () {},
               style: ElevatedButton.styleFrom(
                   minimumSize: Size(300, 50),
                   primary: Colors.deepPurpleAccent,
                   onPrimary: Colors.black,
                   shape:  RoundedRectangleBorder(
                     borderRadius:  BorderRadius.circular(30.0),

                   )
               ),
               child: const Text(
                   "Add a Post",
                   style: TextStyle(
                     fontSize: 26,
                     fontWeight: FontWeight.normal,
                     color: Colors.white,
                   )
               ),

             ),
             SizedBox(height:20,),
             const ListTile(
                 title : Text('Project 1' ,  style: TextStyle(fontWeight: FontWeight.bold) ),
                 subtitle :Text('Lorem Ipsum ')),
             SizedBox(height:00,),
             const ListTile(
                 title : Text('Project 2 ' ,  style: TextStyle(fontWeight: FontWeight.bold) ),
                 subtitle :Text('Lorem Ipsum ')),
             SizedBox(height:00,),
             const ListTile(
                 title : Text('Project 3' ,  style: TextStyle(fontWeight: FontWeight.bold) ),
                 subtitle :Text('Lorem Ipsum ')),

           ],
         ),
        )
    );
  }
  Widget contact() => Column(
    mainAxisSize: MainAxisSize.min,
    children:  [
      SizedBox(height:30,),
      Container(
            child: Ink(
              decoration: BoxDecoration(
                gradient:  const LinearGradient(

                    begin: Alignment.centerRight,
                    end: Alignment.centerLeft,
                    colors: [Colors.deepPurpleAccent,Colors.deepPurpleAccent]
                ),
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Container(
                constraints:  const BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                alignment: Alignment.center,
                child:  const Text("Contact",
                  style: TextStyle(color: Colors.white, fontSize: 26.0, fontWeight:FontWeight.w300),
                ),
              ),
            )
        ),
      SizedBox(height:20,),
      const Text('Linkedin',
          style: TextStyle(
              fontSize: 23,
              fontWeight: FontWeight.normal,
              color: Color(0xFF7779F1)
          )
      ),
      SizedBox(height:10,),
      const Text('Phone : +216 78 745 975',
          style: TextStyle(
              fontSize: 23,
              fontWeight: FontWeight.normal,
              color: Color(0xFF7779F1)
          )
      ),
      SizedBox(height:10,),
      const Text('Email : Contact@societé.tn',
          style: TextStyle(
              fontSize: 23,
              fontWeight: FontWeight.normal,
              color: Color(0xFF7779F1)
          ),

      ),
      SizedBox(height:20,),
    ],
  );

  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => LogInPageFinal()));
  }
}
