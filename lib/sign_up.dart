import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:khademni/user_model.dart';
import 'package:khademni/First_Page.dart';

import 'log_in.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final emailEditingController = new TextEditingController();
  final passwordEditingController =new TextEditingController();
  final confirmPasswordEditingController =new TextEditingController();
  final nameEditingController =new TextEditingController();
  final lastnameEditingController =new TextEditingController();
  final typeEditingController =new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _auth = FirebaseAuth.instance;
  bool x=false ;
  // string for displaying the error Message
  String? errorMessage;




  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Colors.white,

      body: SingleChildScrollView(
        reverse: true,
        child: Column(

          children: [

            Container(
              width: w*0.7,
              height: h*0.35,
              decoration: const BoxDecoration(

                  image: DecorationImage(
                      image: AssetImage(
                          "img/logo.png"
                      ),
                      fit: BoxFit.cover
                  )
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(
                children: [
                   const Text("Sign Up",
                      style: TextStyle(
                        fontSize: 35,
                        fontWeight: FontWeight.normal,
                          color: Color(0xFF7779F1),
                      )
                  ),
                  SizedBox(height:5),
                  Container(
                    decoration:BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 10,
                              offset:Offset(1,1),
                              color: Colors.grey.withOpacity(1)
                          )
                        ]
                    ) ,
                    child: TextFormField(
                      controller: nameEditingController,
                      validator: (value ) {

                        if (value==null ||  value.trim().isEmpty) {
                          return ("First Name cannot be Empty");
                        }

                        return "amine" ;
                      },

                      decoration: InputDecoration(
                          hintText: 'First Name',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),

                          ),
                        suffixIcon: Icon(Icons.account_circle)
                        ),
                      ),
                    ),

                  SizedBox(height:10),
                  Container(
                    decoration:BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 10,
                              offset:Offset(1,1),
                              color: Colors.grey.withOpacity(1)
                          )
                        ]
                    ) ,
                    child: TextFormField(
                      controller: lastnameEditingController,
                      validator: (value) {
                        if (value==null ||  value.trim().isEmpty) {
                          return ("Second Name cannot be Empty");
                        }
                        return "amine";
                      },

                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(

                          hintText: 'Last Name',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          suffixIcon: Icon(Icons.account_circle)
                      ),
                    ),
                  ),
                  SizedBox(height:10,),
                  Container(
                    decoration:BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 10,
                              offset:Offset(1,1),
                              color: Colors.grey.withOpacity(1)
                          )
                        ]
                    ) ,
                    child: TextFormField(
                      controller: emailEditingController,
                      validator: (value) {
                        if (value==null ||  value.trim().isEmpty) {
                          return ("Please Enter Your Email");
                        }
                        // reg expression for email validation
                        if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                            .hasMatch(value)) {
                          return ("Please Enter a valid email");
                        }
                        return "amine";
                      },

                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                          hintText: 'Email',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),

                          ),
                          suffixIcon: Icon(Icons.alternate_email)
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    decoration:BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 10,
                              offset:Offset(1,1),
                              color: Colors.grey.withOpacity(1)
                          )
                        ]
                    ) ,
                    child: TextFormField(
                      
                      controller: passwordEditingController,
                      obscureText: true,
                      validator: (value) {

                        if (value==null ||  value.trim().isEmpty) {
                          return ("Password is required for login");
                        }
                        return "amine";

                      },

                      decoration: InputDecoration(

                          hintText: 'Password',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          suffixIcon: Icon(Icons.password)
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    decoration:BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 10,
                              offset:Offset(1,1),
                              color: Colors.grey.withOpacity(1)
                          )
                        ]
                    ) ,
                    child: TextFormField(

                      controller: confirmPasswordEditingController,
                      obscureText: true,
                      validator: (value) {
                        if (confirmPasswordEditingController.text !=
                            passwordEditingController.text) {
                          return "Password don't match";
                        }
                        return "amine";
                      },

                      decoration: InputDecoration(

                          hintText: 'Confirm Password',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          suffixIcon: Icon(Icons.password)
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  ElevatedButton(onPressed: ()  {
                     signUp(emailEditingController.text, passwordEditingController.text);
                     if (x==true){
                     Navigator.push(context,
                         MaterialPageRoute(
                             builder: (context) =>  const LogInPageFinal()));}

                  },
                    style: ElevatedButton.styleFrom(
                        minimumSize: Size(200, 60),
                        primary: Colors.deepPurpleAccent,
                        onPrimary: Colors.black,
                        shape:  RoundedRectangleBorder(
                          borderRadius:  BorderRadius.circular(30.0),
                        )

                    ),

                    child: const Text(
                        "Sign Up",
                        style: TextStyle(
                          fontSize: 28,
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        )
                    ),

                  ),

                ],
              ),
            ),


          ],
        ),
      ),
    );
  }
  String? _requiredValidator(String? text){
    if(text==null||text.trim().isEmpty){
      return 'this field is required';
    }
    return "jawk behy";
  }
  void postDetailsToFirestore() async {


    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;

    UserModel userModel = UserModel();

    // writing all the values
    userModel.email = user!.email;
    userModel.uid = user.uid;
    userModel.name = nameEditingController.text;
    userModel.lastname = lastnameEditingController.text;

    await firebaseFirestore
        .collection("users")
        .doc(user.uid)
        .set(userModel.toMap());
    Fluttertoast.showToast(msg: "Account created successfully :) ");

    /*Navigator.pushAndRemoveUntil(
        (context),
        MaterialPageRoute(builder: (context) => LoginPage()),
            (route) => false);*/
  }
  void signUp(String email, String password) async {

      try {
        await _auth
            .createUserWithEmailAndPassword(email: email, password: password)
            .then((value) => {postDetailsToFirestore()})
            ;
      } on FirebaseAuthException catch (error) {
        switch (error.code) {
          case "invalid-email":
            errorMessage = "Your email address appears to be malformed.";
            break;
          case "wrong-password":
            errorMessage = "Your password is wrong.";
            break;
          case "user-not-found":
            errorMessage = "User with this email doesn't exist.";
            break;
          case "user-disabled":
            errorMessage = "User with this email has been disabled.";
            break;
          case "too-many-requests":
            errorMessage = "Too many requests";
            break;
          case "operation-not-allowed":
            errorMessage = "Signing in with Email and Password is not enabled.";
            break;
          default:
            errorMessage = "An undefined Error happened.";
        }
        Fluttertoast.showToast(msg: errorMessage!);
        print(error.code);
      }
      Fluttertoast.showToast(
        msg: "Sign up Successful",
        gravity: ToastGravity.CENTER,);
      x=true;

  }

}
